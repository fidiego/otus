from django.conf.urls import url, include
from django.contrib import admin

from monitorables.views import submission_endpoint
from monitorables.views import index_view, diff_view


urlpatterns = [
    url(r'^admin', admin.site.urls),

    url(r'^api/v1/submission', submission_endpoint, name="submission_endpoint"),

    url(r'diffs/(?P<id>\w+)', diff_view, name='diffs'),
    url(r'diffs', diff_view, name='diffs'),

    url(r'', index_view),
]
