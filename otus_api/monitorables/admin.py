from django.contrib import admin
from monitorables.models import MonitoredPage, Watcher


admin.site.register(MonitoredPage)
admin.site.register(Watcher)
