import envoy
import difflib
import logging
import os
import subprocess
import uuid
from hashlib import sha512

from bs4 import BeautifulSoup
from celery.contrib.methods import task
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.postgres.fields import ArrayField
import requests


class BaseModel(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, db_index=True, editable=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Watcher(BaseModel):
    email = models.EmailField()

    def __str__(self):
        return self.email


class PageDiff(BaseModel):
    snapshot_0 = models.ForeignKey('monitorables.PageSnapshot', related_name='diff_0')
    snapshot_1 = models.ForeignKey('monitorables.PageSnapshot', related_name='diff_1')
    diff = ArrayField(models.CharField(max_length=10000))
    page = models.ForeignKey('monitorables.MonitoredPage', related_name='diffs')

    def __str__(self):
        return 'diff for {} {}: {}'.format(self.snapshot_0.page, self.snapshot_0.sha_512[:12], self.snapshot_1.sha_512[:12])


class PageSnapshot(BaseModel):
    sha_512 = models.CharField(max_length=128)
    content = models.CharField(max_length=50000)
    page = models.ForeignKey('monitorables.MonitoredPage', related_name='snapshots')

    def __str__(self):
        return '{} snapshot for {}'.format(self.created_at, self.page)


class MonitoredPage(BaseModel):
    name = models.CharField(max_length=64)
    description = models.CharField(max_length=2000, blank=True, null=True)
    url = models.URLField()
    watchers = models.ManyToManyField('monitorables.Watcher', related_name='watched_pages')
    is_active = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Monitored Page'
        verbose_name_plural = 'Monitored Pages'

    def __str__(self):
        if self.name:
            return self.name
        return self.url

    @task(name='MonitoredPage.fetch')
    def fetch(self):
        command = 'lynx {} -dump'.format(self.url)
        r = envoy.run(command)

        if r.status_code != 0:
            logging.error('Error fetching inital page for {}: {}'.format(self.uuid, self.url))

        response = unicode(r.std_out, errors='replace').strip()
        hashed_content = sha512(response.encode('utf-8')).hexdigest()
        snapshot = PageSnapshot.objects.create(content=response, sha_512=hashed_content, page=self)
        logging.info('Fetched initial snapshot for {}'.format(uuid))
        return snapshot

    @task(name='MonitoredPage.fetch_metadata')
    def fetch_metadata(self):
        if self.name != '' and self.description != '':
            return

        req = requests.get(self.url)

        if not 200 < req.status_code < 300:
            return

        soup = BeautifulSoup(req.content, 'html.parser')
        if not self.name:
            self.name = soup.title.text
        if not self.description:
            desc = soup.find_all('meta', {'name': 'description'})[0].text
            desc = '\n'.join([line for line in desc.split() if line != ''])
            self.description = desc
        self.save()

    @task(name='MonitoredPage.diff')
    def diff(self):
        if self.snapshots.count() < 2:
            return

        snap0, snap1 = list(self.snapshots.all())[-2:]
        snap0_split, snap1_split = snap0.content.split('\n'), snap1.content.split('\n')
        diff_lines = [line for line in difflib.unified_diff(snap0_split, snap1_split)]
        if len(diff_lines) > 0:
            return PageDiff.objects.create(snapshot_0=snap0, snapshot_1=snap1, diff=diff_lines, page=self)


@receiver(post_save, sender=MonitoredPage)
def trigger_fetch_initial(sender, instance, created, **kwargs):
    if created:
        instance.fetch()

@receiver(post_save, sender=MonitoredPage)
def trigger_fetch_metadata(sender, instance, created, **kwargs):
    if created:
        instance.fetch_metadata()