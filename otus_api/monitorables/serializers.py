from rest_framework import serializers

from monitorables.models import MonitoredPage, Watcher


class SubmissionSerializer(serializers.Serializer):
    url = serializers.URLField()
    email = serializers.EmailField()

    def validate_url(self, value):
        return value.strip()

    def validate_email(self, value):
        return value.strip()

    def save(self):
        url = self.validated_data['url']
        email = self.validated_data['email']

        page, created = MonitoredPage.objects.get_or_create(url=url)
        watcher, created = Watcher.objects.get_or_create(email=email)
        page.watchers.add(watcher)
        page.save()
        return page