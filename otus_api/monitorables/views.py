from django.shortcuts import render, get_object_or_404
from django.views.generic import View

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions

from monitorables.models import PageDiff
from monitorables.serializers import SubmissionSerializer


class SubmissionAPIView(APIView):
    def post(self, request):
        serializer = SubmissionSerializer(data=request.data)
        if serializer.is_valid():
            page = serializer.save()
            return Response({'success': True, 'url': page.url, 'name': page.name})
        else:
            return Response({'success': False})
submission_endpoint = SubmissionAPIView.as_view()


class IndexView(View):
    def get(self, request):
        return render(request, 'v1/index.html')
index_view = IndexView.as_view()


class DiffView(View):
    def get(self, request, id=None):
        page_diff = get_object_or_404(PageDiff, pk=id)
        def which_symbol(x):
            if x == '-':
                return 'minus'
            if x == '+':
                return 'plus'
        diff = [(which_symbol(l[0]), l) for l in page_diff.diff if len(l) > 2]
        context = {
            'page': page_diff.page,
            'diff': diff,
            'snapshot_0': page_diff.snapshot_0,
            'snapshot_1': page_diff.snapshot_1,
        }
        return render(request, 'v1/diff.html', context=context)
diff_view = DiffView.as_view()